è proprio bello questo tutorial
# Come resettare la history di una bella repo github (o similare)?

Io ho cercato e [stackoverflow][1] mi è venuta incontro con la seguente soluzione: 

	git checkout --orphan newbranch
	git add -A  # aggiungo tutti i file al nuovo branch e poi commit
	git commit 
	git branch -D master  # cancello il branch principale, master 
	git branch -m master  #rinomino il newbranch in master 
	git push -f origin master    # cancello la storie delle commit anche nel repo remoto 

[1]: http://stackoverflow.com/questions/9683279/make-the-current-commit-the-only-initial-commit-in-a-git-repository
