
![](http://olpc-italia.org/images/olpc-italia-top.gif)

Venerdì prossimo sarò a Firenze per la presentazione del [progetto OLPC Italia](http://olpc-italia.org/home.htm).

Verrà presentato dal suo inventore **Nicholas Negroponte** dell'M.I.T. il famoso laptop da 100 dollari , congiuntamente al **progetto OLPC "Get One , Give One"** a favore degli studenti dei paesi in via di sviluppo.

Ovviamente la mia attitudine tecnica mi porterà ad essere maggiormente attratto dalle **specifiche hardware del nuovo portatile** , ma sono curioso anche di sapere di più sul modello di sviluppo sociale per l'alfabetizzazione dei paesi del sud del mondo.

Sempre che poi non accada come in [Nigeria](http://www.reuters.com/article/oddlyEnoughNews/idUSL1966647020070720). :)
